# Changelog
All notable changes to this job will be documented in this file.

## [0.2.0] - 2023-02-03
* Fail the job based on severity level
* Add `CUSTOM_REGISTRY`, `REGISTRY_USER`, `REGISTRY_PASSWORD` and `CUSTOM_TAG` variables

## [0.1.0] - 2023-02-03
* Initial version