## Objective

This job uses the [Lacework Inline scanner](https://docs.lacework.com/onboarding/integrate-the-lacework-inline-scanner-with-ci-pipelines) to scan your docker image and report the results to Lacework.

## How to use it

1. Set as a variable in your project the following values:
   - `LW_ACCOUNT_NAME` : Your Lacework account name
   - `LW_ACCESS_TOKEN` : Your Lacework access token
2. Copy the job URL located in the `Install` part of the right panel and add it inside the `include` list of your `.gitlab-ci.yml` file (see the [quick setup](/use-the-hub/#quick-setup)). You can specify [a fixed version](#changelog) instead of `latest`.
3. If you need to customize the job (stage, variables, ...) 👉 check the [jobs
   customization](/use-the-hub/#jobs-customization)
4. Well done, your job is ready to work ! 😀

## Variables

| Name | Description | Default |
| ---- | ----------- | ------- |
| `LW_ACCOUNT_NAME` | ⚠️ Mandatory variable. The Lacework account name. This variable should be specified in `GitLab > CI/CD Settings` and be masked. | ` ` |
| `LW_ACCESS_TOKEN` | ⚠️ Mandatory variable. The Lacework access token. This variable should be specified in `GitLab > CI/CD Settings` and be masked. | ` ` |
| `LW_SAVE_REPORT` | Save the report inside the Lacework platform. Can be set to `true` or `false`. | `false` |
| `LW_EXIT_SEVERITY` | Specify an severity level from which the job fails. Options are `LOW`, `MEDIUM`, `HIGH`, `CRITICAL` | `HIGH` |
| `LW_ADDITIONAL_OPTIONS` | Additional options for the scanner. | ` ` |
| `CUSTOM_REGISTRY` | If you use another registry than your GitLab instance's one | ` ` |
| `REGISTRY_USER` | User to use for authenticating `CUSTOM_REGISTRY` | ` ` |
| `REGISTRY_PASSWORD` | Password to use for authenticating `CUSTOM_REGISTRY` | ` ` |
| `CUSTOM_TAG` | If you want to use another tag beside `CI_COMMIT_SHA` or `CI_COMMIT_TAG` | ` ` |
| `IMAGE_TAG` | The default tag for the docker image | `0.15.0` |

## Author
This resource is an **[official job](https://docs.r2devops.io/faq-labels/)** added in [**R2Devops repository**](https://gitlab.com/r2devops/hub) by [@GridexX](https://gitlab.com/gridexx)
