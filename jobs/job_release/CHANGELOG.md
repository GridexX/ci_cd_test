# Changelog
All notable changes to this job will be documented in this file.

## [2.0.0] - 2023-01-25
* 🚨 **BREAKING CHANGE**
  Releases are created based on the metadata file instead of the changelog file.
  Metadata files are retrieved with the `METADATA_FILE_EXTENSION` variable and changelogs are parsed from the metadata files.
* Add `METADATA_FILE_EXTENSION` variable
* Remove `JOBS_DIRECTORY` and `CHANGELOG_FILE` variable

## [1.1.0] - 2022-12-20
* Replace default value of `GITLAB_API_URL` by `${CI_SERVER_HOST}`


## [1.0.0] - 2022-11-23
* Allow to fetch jobs from nested directories
* Use snippet maintained by R2Devops instead of external resource

## [0.1.0] - 2022-10-14
* Initial version
